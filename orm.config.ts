import { DataSourceOptions } from "typeorm";

const config: DataSourceOptions = {
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "Kof99889988",
    database: "testDB",
    entities: ["src/**/*.entity{.ts,.js}"],
    synchronize: false,
};
export default config;