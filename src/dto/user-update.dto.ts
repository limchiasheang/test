export class updateUserDto {
    username: string;
    password: string;
    email: string;
    role: boolean
}