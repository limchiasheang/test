export class createUserDto {
    username: string;
    password: string;
    email: string;
    role: boolean
}