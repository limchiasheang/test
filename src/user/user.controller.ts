import { Controller, Get, Post, Put, Delete, Param, Body, NotFoundException, ForbiddenException, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { RoleGuard } from '../role/role.guard';
import { Roles } from '../role/role.decorator';

@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Roles('admin')
    @UseGuards(JwtAuthGuard, RoleGuard)
    @Get(':id')
    async getUser(@Param('id') id: number) {
        const user = await this.userService.findOneById(id);

        if (!user) {
            throw new NotFoundException(`User with ID ${id} not found`);
        }
        return user;
    }

    @UseGuards(JwtAuthGuard)
    @Post()
    async createUser(@Body() createUserDto) {
        let existingData = await this.userService.findOne(createUserDto.username)
        if (!existingData)
            return this.userService.createUser(createUserDto.username, createUserDto.password, createUserDto.email, createUserDto.role);
        else
            throw new ForbiddenException(`Username with '${createUserDto.username}' already exist`);
    }

    @Roles('admin')
    @UseGuards(JwtAuthGuard, RoleGuard)
    @Put(':id')
    async updateUser(@Param('id') id: any, @Body() updateUserDto) {
        return this.userService.updateUser(id, updateUserDto.username, updateUserDto.password, updateUserDto.email, updateUserDto.role);
    }

    @Roles('admin')
    @UseGuards(JwtAuthGuard, RoleGuard)
    @Delete(':id')
    async deleteUser(@Param('id') id: number) {
        return this.userService.deleteUser(id);
    }
}
