import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';

@Injectable()
export class UserService {
    @InjectRepository(User)
    private readonly userRepository: Repository<User>

    constructor(
    ) { }

    async findOne(username: string): Promise<User | undefined> {
        return this.userRepository.findOne({ where: { username } });
    }

    async findOneById(id: any): Promise<User | undefined> {
        return this.userRepository.findOne({
            where: { id }
        });
    }

    async createUser(username: string, password: string, email: string, role: string): Promise<User> {
        const user = new User();
        user.username = username;
        user.password = password;
        user.email = email;
        user.role = role;
        return this.userRepository.save(user);
    }

    async updateUser(id: any, username: string, password: string, email: string, role: string): Promise<User> {
        const user = await this.userRepository.findOne({
            where: { id }
        });
        if (!user) {
            throw new NotFoundException(`User with ID ${id} not found`);
        }

        user.username = username;
        user.password = password;
        user.email = email;
        user.role = role;

        return this.userRepository.save(user);
    }

    async deleteUser(id: any): Promise<void> {
        const user = await this.userRepository.findOne({
            where: { id }
        });
        if (!user) {
            throw new NotFoundException(`User with ID ${id} not found`);
        }
        await this.userRepository.remove(user);
    }
}
