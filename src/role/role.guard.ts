import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RoleGuard implements CanActivate {
  // canActivate(
  //   context: ExecutionContext,
  // ): boolean | Promise<boolean> | Observable<boolean> {
  //   return true;
  // }
  constructor(private readonly reflector: Reflector) { }

  canActivate(context: ExecutionContext): boolean {

    const requiredRoles = this.reflector.get<string[]>('roles', context.getHandler());

    if (!requiredRoles || requiredRoles.length === 0) {
      // No roles are required, so access is allowed
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const userRole = request.user.role; // Assuming "role" is in the JWT payload

    if (requiredRoles.includes(userRole)) {
      // User has one of the required roles, access is allowed
      return true;
    }

    // User does not have any of the required roles, access is denied
    return false;
  }
}
